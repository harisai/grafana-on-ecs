# Grafana on ECS

This module sets up Grafana on ECS using Grafana docker image and stores data on a given database.

## How to

1. Config this module
2. Run Terraform
3. Get Grafana password from Parameter Store
4. Log into database and run the sql queries below adding the password for Grafana.

```sh
terraform init
cp terraform.tfvars.model terraform.tfvars
# fill terraform.tfvars
# or change some of the strategies used for the configuration based on your needs
terraform plan -out=tfplan
terraform apply tfplan
```

## Setup your DB

For Grafana to work properly using ECS you need to setup your 
database with the following script:

```
CREATE DATABASE grafana;
CREATE USER grafana WITH ENCRYPTED PASSWORD 'your_grafana_password';
GRANT ALL PRIVILEGES ON DATABASE grafana TO grafana;
```

## Secrets

The Task Definition will get the environment vars for the Grafana Container
on the Systems Manager Parameter Store. Check the `locals.tf` to see how the variables are constructed.

## Module Setup

```
module "grafana" {
  source              = "git@gitlab.com:eudevops/grafana.git?ref=main"
  app_name            = ""
  cluster_name        = ""
  database_name       = ""
  desired_task_cpu    = 1024
  desired_task_memory = 2048
  domain              = ""
  grafana_port        = 3000
  is_https            = false
  launch_type         = "FARGATE"
  load_balancer_name  = ""
  region              = "us-east-1"
  vpc_id              = ""
}
```

## Authentication

You can check the [Grafana Cloudwatch](https://grafana.com/docs/grafana/latest/features/datasources/cloudwatch/) docs
to see how the integration with Cloudwatch. This module already sets up the IAM Role for Grafana that you can use for Assume Role connection to Cloudwatch. However, I couldn't config this role as an environment variable because the role is defined inside
the ecs module, so it would give a circular dependence. The ecs module outputs the arn of the IAM Role that you can get using Terraform Console and other tools to configure Grafana directly on the board.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.61.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_grafana"></a> [grafana](#module\_grafana) | git@gitlab.com:eudevops/ecs-service.git | main |

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.ecs_ssm_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.grafana](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_kms_alias.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_route53_record.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_ssm_parameter.db_host](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.db_name](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.db_pass](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.db_type](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.db_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [random_password.grafana_pass](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_db_instance.database](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/db_instance) | data source |
| [aws_iam_policy_document.grafana](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ssm_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_lb.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_route53_zone.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_app_name"></a> [app\_name](#input\_app\_name) | Application name that this grafana will monitor | `string` | n/a | yes |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | ECS cluster to deploy the service | `string` | n/a | yes |
| <a name="input_database_name"></a> [database\_name](#input\_database\_name) | Name of the postgres database that grafana will use | `string` | n/a | yes |
| <a name="input_desired_task_cpu"></a> [desired\_task\_cpu](#input\_desired\_task\_cpu) | Task CPU Limit | `number` | `1024` | no |
| <a name="input_desired_task_memory"></a> [desired\_task\_memory](#input\_desired\_task\_memory) | Task Memory Limit | `number` | `2048` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | Base domain to deploy grafana dashboard access | `string` | n/a | yes |
| <a name="input_grafana_port"></a> [grafana\_port](#input\_grafana\_port) | Port tha grafana should expose | `number` | `3000` | no |
| <a name="input_is_https"></a> [is\_https](#input\_is\_https) | Grafana will be exposed with https? | `bool` | `false` | no |
| <a name="input_launch_type"></a> [launch\_type](#input\_launch\_type) | ECS Service launch type | `string` | `"FARGATE"` | no |
| <a name="input_load_balancer_name"></a> [load\_balancer\_name](#input\_load\_balancer\_name) | Name of the load balancer to deploy the app | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | Aws region | `string` | `"us-east-1"` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | Id of te vpc to deploy the application | `string` | `""` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
