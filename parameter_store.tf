resource "aws_ssm_parameter" "db_type" {
  name  = "/${var.app_name}/grafana/GF_DATABASE_TYPE"
  type  = "String"
  value = data.aws_db_instance.database.engine
}

resource "aws_ssm_parameter" "db_host" {
  name  = "/${var.app_name}/grafana/GF_DATABASE_HOST"
  type  = "String"
  value = data.aws_db_instance.database.address
}

resource "aws_ssm_parameter" "db_user" {
  name  = "/${var.app_name}/grafana/GF_DATABASE_USER"
  type  = "String"
  value = "grafana"
}

resource "aws_ssm_parameter" "db_name" {
  name  = "/${var.app_name}/grafana/GF_DATABASE_NAME"
  type  = "String"
  value = "grafana"
}

resource "random_password" "grafana_pass" {
  length  = 64
  special = false
}

resource "aws_ssm_parameter" "db_pass" {
  name   = "/${var.app_name}/grafana/GF_DATABASE_PASSWORD"
  type   = "SecureString"
  key_id = aws_kms_key.this.id
  value  = random_password.grafana_pass.result
}
